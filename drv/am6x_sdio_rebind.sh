#!/bin/sh

CC33XX_MMC_DEVICE="fa20000.mmc"

echo  "$CC33XX_MMC_DEVICE" > /sys/bus/platform/drivers/sdhci-am654/unbind 
sleep 1
echo  "$CC33XX_MMC_DEVICE" > /sys/bus/platform/drivers/sdhci-am654/bind 

# Workaround for systemd-networkd not initializing default STA role after
# SDIO rebind (root-cause still pending).
sleep 3
systemctl restart systemd-networkd
echo "Done"